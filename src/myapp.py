# -*- coding:utf-8 -*-

import cv2
import numpy as np

# 入力画像を読み込み
img = cv2.imread('../img/1.jpg')

# グレースケール変換
gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

dst3 = cv2.GaussianBlur(gray, ksize=(3, 3), sigmaX=1.3)

ret, thresh1 = cv2.threshold(dst3, 127, 255, cv2.THRESH_BINARY)
ret, thresh2 = cv2.threshold(dst3, 127, 255, cv2.THRESH_BINARY_INV)
ret, thresh3 = cv2.threshold(dst3, 127, 255, cv2.THRESH_TRUNC)
ret, thresh4 = cv2.threshold(dst3, 127, 255, cv2.THRESH_TOZERO)
ret, thresh5 = cv2.threshold(dst3, 127, 255, cv2.THRESH_TOZERO_INV)

images = [dst3, thresh1, thresh2, thresh3, thresh4, thresh5]
index = 0

# 3x3 で全要素が1である構造要素を作成する。
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

for img in images:
    # 2値画像を収縮する。
    dst = cv2.erode(thresh2, kernel)
    # 結果を出力
    cv2.imwrite('../img/モルフォロジー' + str(index) + '.jpg', img)
    index += 1
